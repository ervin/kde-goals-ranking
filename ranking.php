#! /usr/bin/env php
<?php

require_once "Condorcet/__CondorcetAutoload.php";
use CondorcetPHP\Condorcet\Election;

function candidate_name_to_id($name) {
    return trim(substr($name, 0, Election::MAX_LENGTH_CANDIDATE_ID));
}

function create_single_candidate_id_to_name_map($name) {
    return [candidate_name_to_id($name) => $name];
}

function create_candidate_names_map($names) {
    $array_of_maps = array_map('create_single_candidate_id_to_name_map', $names);
    return array_reduce($array_of_maps, 'array_merge', array());
}


# Was a file name passed on the command line?
$argv = $_SERVER["argv"];
if (count($argv) !== 2) {
    print "Please invoke me pointing to one (and only one) CSV file\n";
    exit(1);
}

# Open the file
$handle = fopen($argv[1], "r");
if (!$handle) {
    print "Couldn't open $argv[1]";
    exit(1);
}

# Create the election
$election = new Election();


# Discard the first line (headers) of the file
fgets($handle);


# Find the candidates from the first real line
$line = fgetcsv($handle);

$candidate_names_map = create_candidate_names_map(array_slice($line, 1));
foreach (array_keys($candidate_names_map) as $candidate_id) {
    $election->addCandidate($candidate_id);
}


# Get all the votes in, one line at a time
while ($line) {
    $vote_tag = "Vote $line[0]";
    $vote = array_map('candidate_name_to_id', array_slice($line, 1));

    $election->addVote($vote, $vote_tag);

    $line = fgetcsv($handle);
}

fclose($handle);


# We'll need to get back to the proper candidate names
$candidate_id_to_name = function($candidate) use ($candidate_names_map) {
    return $candidate_names_map[$candidate->getName()];
};


# Display the results
print "-- Results --\n";
foreach ($election->getResult("Schulze") as $rank => $candidates) {
    print "Rank " . $rank . ": ";
    echo implode(",",array_map($candidate_id_to_name, $candidates));
    echo "\n";
}

?>
