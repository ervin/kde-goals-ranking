The code used for ranking KDE goals since 2019

This assumes PHP 7 on the command line, and the mbstring module available,
this also uses a submodule to pull the condorcet dependency.

Note that it will simply die with an exception if a bogus entry (typically and
incomplete answer) is in the data set. Make sure to use it only with exports
containing only complete answers.
